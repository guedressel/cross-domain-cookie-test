# Cross Domain Cookie

Test to see if cookies get sent by a browser to a PHP server - cross domain.

This is why: https://webkit.org/tracking-prevention/#intelligent-tracking-prevention-itp


## License
WTFPL - see license.txt

## Project status
Done. No further development to be expected.
