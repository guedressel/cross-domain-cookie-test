<?php

define('DOMAIN_A', 'domain-a');
define('DOMAIN_B', 'domain-b');

header("Cache-Control: max-age=0 no-cache no-storage");

if( empty( $_COOKIE ) ) {
  $cookie_info = 'nothing';
} else {
  $cookie_info = "";
  foreach( $_COOKIE as $key => $val ) {
    $cookie_info .= "$key=$val ";
  }
}

session_start();

if( ! isset($_SESSION['counter']) ) {
  $_SESSION['counter'] = 0;
}
$req_count = $_SESSION['counter']++;
$req_host = $_SERVER['HTTP_HOST'];

$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
$cross_req_url = "$scheme://" . DOMAIN_B . "/cross-request.php";

?><html>
<head>
<script>

function crossRequest(e) {
  let btn = document.getElementById('cross-request-btn');
  let element = document.getElementById('cross-request-container');
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function (e) {
    switch(xhr.readyState) {
      case 1:
        btn.disabled = true;
        element.innerHTML = "request in progress...";
        break;
      case 4:
        btn.disabled = false;
        if (xhr.status == 200) {
          element.innerHTML = xhr.responseText;
        } else {
          element.innerHTML = "Request failed";
          console.error("Bad XHR Status",xhr);
        }
        break;
    }
  }
  xhr.open("GET", "<?php echo "$cross_req_url"; ?>", true);
  xhr.withCredentials = true;
  xhr.send();
}

</script>
<style>
object#cross-request-obj {
  width: 100%;
  height: 30em;
}
</style>
</head>
<body>
<h1>Cookie Test</h1>
<p>
Request Host: <?php echo "$req_host"; ?><br>
Request contained cookie(s): <?php echo "$cookie_info"; ?><br>
Session ID: <?php echo session_id(); ?><br>
Session Request Counter: <?php echo "$req_count"; ?><br>
</p>
<h3>Cross Request</h3>
<p>URL: <a href="<?php echo "$cross_req_url"; ?>"><?php echo "$cross_req_url"; ?></a></p>

<h4>XMLHttpRequest</h4>
<button id="cross-request-btn" onclick="crossRequest();">Trigger request</button>
<pre id="cross-request-container">...</pre>
<script>crossRequest()</script>

<h4>Object Embed</h4>
<object id="cross-request-obj" type="text/plain" data="<?php echo "$cross_req_url"; ?>" ></object>

</body>
</html>
