<?php 

if( empty( $_COOKIE ) ) {
  $cookie_info = "  ¯\_(ツ)_/¯   no cookie found \n";
} else {
  $cookie_info = "";
  foreach( $_COOKIE as $key => $val ) {
    $cookie_info .= "$key=$val\n";
  }
}

if( ! isset($_SESSION['counter']) ) {
  $_SESSION['counter'] = 0;
}
$req_count = $_SESSION['counter']++;
$req_host = $_SERVER['HTTP_HOST'];


?>Session ID: <?php echo session_id() . "\n"; ?>
Counter: <?php echo "$req_count\n"; ?>
Host: <?php echo "$req_host\n"; ?>
-----
Cookies found in request:
<?php echo $cookie_info; ?>
-----
