<?php

define('DOMAIN_A', 'domain-a');
define('DOMAIN_B', 'domain-b');

header("Cache-Control: max-age=0 no-cache no-storage");
session_start();

$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
$site_a_url = "$scheme://" . DOMAIN_A . "/";

?><html>
<head>
</head>
<body>
<h1>Cookie Test</h1>
<p>Please head over to <a href="<?php echo $site_a_url; ?>"><?php echo DOMAIN_A; ?></a> to run this test.</p>
<pre>
<?php require('cookie-info.inc'); ?>
</pre>
</body>
</html>
