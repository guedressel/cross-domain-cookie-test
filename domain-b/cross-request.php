<?php 

define('COOKIE_SHARE_ROOT_DOMAIN', true);

header("Content-Type: text/plain");
header("Cache-Control: max-age=0 no-cache no-storage");

// CORS
if( $referrer_host = parse_url($_SERVER['HTTP_REFERER'] ?? "", PHP_URL_HOST) ) {
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
  header("Access-Control-Allow-Origin: $scheme://$referrer_host");
  header("Access-Control-Allow-Credentials: true");
}

$domain = $_SERVER['HTTP_HOST'];
if( COOKIE_SHARE_ROOT_DOMAIN ) {
  $domain_parts = explode('.', $_SERVER['HTTP_HOST']);
  if( count($domain_parts) > 2 ) {
    $tld = array_pop($domain_parts);
    $name = array_pop($domain_parts);
    $domain = "$name.$tld";
  }
  $domain = ".$domain";
}

session_set_cookie_params([
  'lifetime' => 0,
  'path' => '/',
  'domain' => $domain,
  'secure' => true,
  'httponly' => true,
  'samesite' => 'None'
]);
session_start();

require('cookie-info.inc');
